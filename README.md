## Overview

This repository contains files needed to deploy the lwcs used on the Order lightning record page.
These components are used to add and display order products added to the order by a user.

---

## Prerequisite
1. SFDX CLI has been installed
2. Knowledge of Salesforce

## Usage

1. Clone the repo into a directory on local machine
2. Navigate to the repo and run the command sfdx force:source:deploy -x path\to\package.xml -u usernameoralias -l RunSpecifiedTests -r TEST_CTRL_OrderProducts,TEST_CTRL_ProductList,TEST_API_SendActivatedOrder -c
3. On success, run the following command sfdx force:source:deploy -x path\to\package.xml -u usernameoralias -l RunSpecifiedTests -r TEST_CTRL_OrderProducts,TEST_CTRL_ProductList,TEST_API_SendActivatedOrder
4. This will deploy the code to the target org

Once the code has been deployed there are some post deployment steps to be done
1. Navigate to the Order object in Salesforce
2. Open the LP_Order lightning page and set it as the org default page
3. Verify that the remote site setting URL is https://sfwolforder.requestcatcher.com

---

## Assumptions and UI usage

Assumptions are that a Contract needs to be created first linked to a pricebook before creating the order record.
1. Navigate to the Contract tab in Salesforce (from the app launcher)
2. Create a contract and add required fields including the pricebook
3. Create an Order record and link it to the Contract and populate required fields
4. On the Order page 2 components should be visible. Add some products by selecting the checkboxes and clicking "Add Products" button.
5. Order products will be created and added to the Order.
6. To Activate the order click on the "Activate" button. This will set the Order in a status of Activated as well as the status of the order products


