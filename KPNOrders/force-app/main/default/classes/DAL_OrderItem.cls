/**
 * @description This is a Data Access Layer (DAL) Class to get any Order item information
 * @author Minal
 * @date 20210614
 */
global class DAL_OrderItem {

    /**
     * @param SFrecordId The Order Id
     * @description This method returns all order products in an Order
     * @return List of Order Item
     */
    public static List<OrderItem> getProductsByOrder(Id SFrecordId) {
        return [
                SELECT
                        Id,
                        OrderItemNumber,
                        UnitPrice,
                        ListPrice,
                        Quantity,
                        TotalPrice,
                        Product2.Name,
                        Product2.Id,Product2Id,
                        Status__c,
                        Product2.ProductCode
                FROM OrderItem
                WHERE OrderId = :SFrecordId];
    }
}