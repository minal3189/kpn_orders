/**
 * @description This class is used to wrap the order and its product order for a callout
 * @author Minal
 * @date 20210615
 */
public class DTO_OrderProducts {

        public String accountNumber;
        public String orderNumber;
        public String type;
        public String status;
        public List<OrderProducts> orderProducts;

        public class OrderProducts {
            public String name;
            public String code;
            public Double unitPrice;
            public Integer quantity;
        }
}