/**
 * @description This class is used to make a call out to the 3rd party system for activated orders
 * @author Minal
 * @date 20210615
 */
public class API_SendActivatedOrder {

    public static final String POST_METHOD = 'POST';

    /**
     * @param endpoint The endpoint to set
     * @param body the request body
     *
     * @return response
     */
    //@Future(Callout = true)
    public static Boolean makePostCallout(String endpoint, String body) {
        Boolean isSuccess;

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod(POST_METHOD);
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody(body);
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() == 200) {
            System.debug('!!!SUCCESS!!!!!');
           isSuccess = true;
        } else {
            System.debug('!!!FAILURE!!!!!');
            isSuccess = false;
        }

        return isSuccess;
    }

}