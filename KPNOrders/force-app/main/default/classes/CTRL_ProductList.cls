/**
 * @description This class is used as a controller class with functionality to add products to an order
 * @author Minal Rama
 * @date 20210614
 */
global class CTRL_ProductList {

    /**
     * @param SForderId The Order Id
     * @description Gets all the products and entries from pricebook of the given order
     * @return List of PricebookEntry
     */
    @AuraEnabled(Cacheable=true)
    public static List<PricebookEntry> getProductsAndEntriesByOrder(Id SForderId) {

        return DAL_PricebookEntry.getAvailableProductsFromPriceBook(SForderId);
    }

    /**
     * @param SForderId The Order Id
     * @param orderProductList List of pricebook entries to add to the order
     * @description The method adds OrderItems to the Order record based on what was selected in the UI
     * @return Boolean true if items found and upsert successful or false if failure or list is empty
     */
    @AuraEnabled
    public static Boolean addProductsToOrder(Id SForderId, List<PricebookEntry> orderProductList) {
        Boolean isSuccess;

        //Check if the incoming list from the front end is not empty
        if (!orderProductList.isEmpty()) {
            //Get all products from order and loop through them to see if any selected products are contained in them
            List<OrderItem> currentOrderItems = DAL_OrderItem.getProductsByOrder(SForderId);
            List<OrderItem> orderItemsToUpsert = new List<OrderItem>();

            //Map used for getting existing products in the order
            Map<Id, OrderItem> productMap = new Map<Id, OrderItem>();

            for (OrderItem item : currentOrderItems) {
                productMap.put(item.Product2Id, item);
            }

            //Loop through selected products based on what was selected in the UI
            //and compare them the the already existing products in the order. If found increment quantity by 1
            for (PricebookEntry entry : orderProductList) {
                if (productMap.get(entry.Product2Id) != null) {
                    if (productMap.get(entry.Product2Id).Product2Id == entry.Product2Id) {
                        OrderItem item = productMap.get(entry.Product2Id);
                        item.Quantity += 1;
                        orderItemsToUpsert.add(item);
                    }
                } else {
                    // if not found create new OrderItem
                    OrderItem newOrderItem = new OrderItem();
                    newOrderItem.OrderId = SForderId;
                    newOrderItem.Product2Id = entry.Product2Id;
                    newOrderItem.PricebookEntryId = entry.Id;
                    newOrderItem.UnitPrice = entry.UnitPrice;
                    newOrderItem.Quantity = 1;
                    orderItemsToUpsert.add(newOrderItem);

                }
            }

            if (!orderItemsToUpsert.isEmpty()) {
                try {
                    upsert orderItemsToUpsert;
                    isSuccess = true;
                } catch (Exception e) {
                    isSuccess = false;
                    System.debug('Error upserting records ' + e);
                }
            }
        } else {
            isSuccess = false;
            System.debug('There are no order items');
        }
        return isSuccess;
    }
}