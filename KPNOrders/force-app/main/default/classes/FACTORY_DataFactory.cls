/**
 * @description Factory Class used by test classes to generate data
 * @author Minal
 * @date 20210614
 */
public class FACTORY_DataFactory {
    /**
     * @description creates and inserts accounts to DB
     */
    public static void createAccounts() {

        List<Account> accountsToInsert = new List<Account>();

        for (Integer i = 0; i < 5; i++) {
            Account acc = new Account(Name = 'TestAccount' + i);
            accountsToInsert.add(acc);
        }
        insert accountsToInsert;
    }

    /**
     * @param acc The account to associate the contract to
     * @param status the status for the contract
     * @param startDate start date of contract
     * @param term term of contract
     * @param priceBookId the pricebook associated to the contract
     *
     * @return List Contract
     */
    public static List<Contract> createContract(Account acc, String status, Date startDate, Integer term, Id priceBookId) {

        List<Contract> contractsToInsert = new List<Contract>();
        Contract con = new Contract(
                AccountId = acc.Id,
                Status = status,
                StartDate = startDate,
                ContractTerm = term,
                Pricebook2Id = priceBookId
        );

        contractsToInsert.add(con);

        insert contractsToInsert;
        return contractsToInsert;
    }

    /**
     * @description Sets up some products and pricebook entries
     */
    public static void SetupProductsAndPricebooks() {


        Product2 product1 = new Product2(Name = 'iPhone', Family = 'Mobile');
        Product2 product2 = new Product2(Name = 'DELL', Family = 'Laptop');

        Insert product1;
        Insert product2;

        Pricebook2 standardPricebook = new Pricebook2(
                Id = Test.getStandardPricebookId(),
                IsActive = true
        );

        Update standardPricebook;

        standardPricebook = [SELECT Id, IsStandard FROM Pricebook2 WHERE Id = :standardPricebook.Id];
        System.assertEquals(true, standardPricebook.IsStandard);

        PricebookEntry pbe1 = new PricebookEntry(
                Pricebook2Id = standardPricebook.Id,
                Product2Id = product1.Id,
                UnitPrice = 1020,
                IsActive = true
        );
        Insert pbe1;

        PricebookEntry pbe2 = new PricebookEntry(
                Pricebook2Id = standardPricebook.Id,
                Product2Id = product2.Id,
                UnitPrice = 2000,
                IsActive = true
        );
        Insert pbe2;

        pbe1 = [SELECT Id, Pricebook2.IsStandard FROM PricebookEntry WHERE Id = :pbe1.Id];
        System.assertEquals(true, pbe1.Pricebook2.IsStandard);
    }

    /**
     * @param acc accounts order
     * @param status status for the order
     * @param startDate start date for the order
     * @param contractId contract associated to the order
     * @description Creates an order record
     *
     * @return List Order
     */
    public static List<Order> createOrder(Account acc, String status, Date startDate, Id contractId) {

        List<Order> ordersToInsert = new List<Order>();
        Order o = new Order(
                AccountId = acc.Id,
                EffectiveDate = startDate,
                Status = status,
                ContractId = contractId
        );

        ordersToInsert.add(o);

        insert ordersToInsert;
        return ordersToInsert;
    }

    /**
     * @param orderId order id
     * @param productId product Id
     * @param entryId entry Id
     * @description creates order products for an order
     */
    public static void createOrderProducts(Id orderId, Id productId, Id entryId) {
        OrderItem newOrderItem = new OrderItem(
                OrderId = orderId,
                Product2Id = productId,
                PricebookEntryId = entryId,
                UnitPrice = 500,
                Quantity = 1
        );

        insert newOrderItem;

    }

}