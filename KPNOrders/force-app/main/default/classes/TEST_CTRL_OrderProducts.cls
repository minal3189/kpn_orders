/**
 * @description Test class for testing the CTRL_OrderProducts
 */
@IsTest
public class TEST_CTRL_OrderProducts {
    private static final String DRAFT_STATUS = 'Draft';
    private static final String ACTIVATED_STATUS = 'Activated';
    private static final Integer CONTRACT_TERM = 24;

    /**
     * @description setup pricebooks and accounts
     */
    @TestSetup
    static void makeData() {

        FACTORY_DataFactory.createAccounts();
        FACTORY_DataFactory.SetupProductsAndPricebooks();

    }

    /**
     * @description Test getting the order items
     */
    @IsTest
    static void test_getOrderItems() {

        setupContractAndOrder();
        Order ord = [SELECT Id FROM Order LIMIT 1];
        List<OrderItem> items = CTRL_OrderProducts.getOrderItems(ord.Id);

        //Should return empty list since we didnt add any products to the order
        System.assertEquals(true, items.isEmpty(), 'Items are not empty');
    }

    /**
     * @description Test activating the order
     */
    @IsTest
    static void test_activateOrderAndOrderItems() {

        setupContractAndOrder();

        Order ord = [SELECT Id FROM Order LIMIT 1];
        Product2 product = [SELECT Id FROM Product2 LIMIT 1];
        PricebookEntry priceEntry = [SELECT Id FROM PricebookEntry WHERE Product2Id = :product.Id LIMIT 1];

        FACTORY_DataFactory.createOrderProducts(ord.Id, product.Id, priceEntry.Id);

        List<OrderItem> items = CTRL_OrderProducts.getOrderItems(ord.Id);

        //Should return list with data
        System.assertEquals(true, !items.isEmpty(), 'Items are empty');

        try {
            Boolean result = CTRL_OrderProducts.activateOrderAndOrderItems(ord.Id);

        } catch (Exception e) {

            System.assert(true, 'Exception not thrown');

        }

        //Update the contract status before activating the order
        Contract contr = [SELECT Id, Status FROM Contract LIMIT 1];
        contr.Status = ACTIVATED_STATUS;
        update contr;

        //Now try activate the order and products
        try {
            Boolean result = CTRL_OrderProducts.activateOrderAndOrderItems(ord.Id);
            System.assertEquals(true, result, 'The result from the call is false, an error occured');

            //Check the status of the Order, it should be activated
            ord = [SELECT Id, Status FROM Order LIMIT 1];
            System.assertEquals(ACTIVATED_STATUS, ord.Status, 'The Status is not activated');

        } catch (Exception e) {

            System.assert(true, 'Exception not thrown');

        }


    }

    /**
     * @description Sets up contracts and order
     */
    private static void setupContractAndOrder() {

        Account selectedAccount = [SELECT Id, Name FROM Account LIMIT 1];
        Id pbID = Test.getStandardPricebookId();
        Contract contr = FACTORY_DataFactory.createContract(selectedAccount, DRAFT_STATUS, System.today(), CONTRACT_TERM, pbID)[0];
        Order ord = FACTORY_DataFactory.createOrder(selectedAccount, DRAFT_STATUS, System.today(), contr.Id)[0];
    }


}