/**
 * @description This is a Data Access Layer (DAL) Class to get any Order related information
 * @author Minal
 * @date 20210614
 */
public class DAL_Order {

    /**
     * @param SFrecordId The order Id
     * @description This method returns an order by id
     * @return List Order
     */
    public static List<Order> getOrderById(Id SFrecordId){ 
        return [
                SELECT
                        Id,
                        Status,
                        Contract.Status,
                        ActivatedById,
                        ActivatedDate,
                        Name,
                        Contract.Pricebook2Id,
                        Account.AccountNumber,
                        OrderNumber,
                        Type
                FROM Order
                WHERE Id =: SFrecordId];
    }
}