/**
 * @description This is a Data Access Layer (DAL) Class to get any pricebook entry information
 * @author Minal
 * @date 20210614
 */
public class DAL_PricebookEntry {
    /**
     * @param SFOrderId The Order Id
     * @description Gets all the pricebook entries and products based on the contracts pricebook
     * @return List of PricebookEntry
     */
    public static List<PricebookEntry> getAvailableProductsFromPriceBook(Id SFOrderId){
        List<Order> orders = DAL_Order.getOrderById(SFOrderId);

        return [
                SELECT Id,
                        Product2.Name,
                        UnitPrice
                FROM PricebookEntry
                WHERE Pricebook2Id =: orders[0].Contract.Pricebook2Id
                ORDER BY Product2.Name ASC LIMIT 1000 ];

    }
}