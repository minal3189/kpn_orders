/**
 * Created by MinalRama on 2021/06/15.
 */
@IsTest
global class TEST_API_SendActivatedOrder implements HttpCalloutMock {

    /**
     * @param request request to be made
     * @description class to mock callout
     * @return HttpResponse
     */
    global HttpResponse respond(HttpRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"type":null,"status":"Activated","orderProducts":[{"unitPrice":25000.0,"quantity":3,"name":"GenWatt Diesel 200kW","code":"GC1040"},{"unitPrice":100000.0,"quantity":1,"name":"GenWatt Diesel 1000kW","code":"GC1060"},{"unitPrice":5000.0,"quantity":1,"name":"GenWatt Diesel 10kW","code":"GC1020"},{"unitPrice":150000.0,"quantity":1,"name":"GenWatt Gasoline 2000kW","code":"GC5060"},{"unitPrice":35000.0,"quantity":1,"name":"GenWatt Gasoline 300kW","code":"GC5020"},{"unitPrice":75000.0,"quantity":1,"name":"GenWatt Gasoline 750kW","code":"GC5040"},{"unitPrice":15000.0,"quantity":1,"name":"GenWatt Propane 100kW","code":"GC3020"},{"unitPrice":120000.0,"quantity":1,"name":"GenWatt Propane 1500kW","code":"GC3060"},{"unitPrice":50000.0,"quantity":1,"name":"GenWatt Propane 500kW","code":"GC3040"},{"unitPrice":85000.0,"quantity":1,"name":"Installation: Industrial - High","code":"IN7080"}],"orderNumber":"00000100","accountNumber":"1234567898"}');
        response.setStatusCode(200);
        return response;
    }

    /**
     * @description test method to do a mock callout to endpoint with 200 OK
     */
    @IsTest static void testPostCallout() {
        String jsonString = '{"type":null,"status":"Activated","orderProducts":[{"unitPrice":25000.0,"quantity":3,"name":"GenWatt Diesel 200kW","code":"GC1040"},{"unitPrice":100000.0,"quantity":1,"name":"GenWatt Diesel 1000kW","code":"GC1060"},{"unitPrice":5000.0,"quantity":1,"name":"GenWatt Diesel 10kW","code":"GC1020"},{"unitPrice":150000.0,"quantity":1,"name":"GenWatt Gasoline 2000kW","code":"GC5060"},{"unitPrice":35000.0,"quantity":1,"name":"GenWatt Gasoline 300kW","code":"GC5020"},{"unitPrice":75000.0,"quantity":1,"name":"GenWatt Gasoline 750kW","code":"GC5040"},{"unitPrice":15000.0,"quantity":1,"name":"GenWatt Propane 100kW","code":"GC3020"},{"unitPrice":120000.0,"quantity":1,"name":"GenWatt Propane 1500kW","code":"GC3060"},{"unitPrice":50000.0,"quantity":1,"name":"GenWatt Propane 500kW","code":"GC3040"},{"unitPrice":85000.0,"quantity":1,"name":"Installation: Industrial - High","code":"IN7080"}],"orderNumber":"00000100","accountNumber":"1234567898"}';
        String endpoint = CTRL_OrderProducts.WOLF_ENDPOINT;
        Test.setMock(HttpCalloutMock.class, new TEST_API_SendActivatedOrder());
        Boolean responseResult = API_SendActivatedOrder.makePostCallout(endpoint, jsonString);
        System.assertEquals(true, responseResult);
    }

}