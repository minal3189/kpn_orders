/**
 * @description This class is used as a controller class with methods to active the order and order products
 * @author Minal Rama
 * @date 20210614
 */
global class CTRL_OrderProducts {

    private static final String STATUS_ACTIVATED = 'Activated';
    @TestVisible
    private static final String WOLF_ENDPOINT = 'https://sfwolforder.requestcatcher.com/test';

    /**
     * @param SForderId The Order Id
     * @description method to get all the order items based on order Id
     * @return List of Order Items
     */
    @AuraEnabled(Cacheable=true)
    public static List<OrderItem> getOrderItems(Id SForderId) {
        return DAL_OrderItem.getProductsByOrder(SForderId);
    }

    /**
     * @param SForderId The Order Id
     * @description Method that activates the order and order products by setting the status to Activated
     * @return Boolean
     */
    @AuraEnabled
    public static Boolean activateOrderAndOrderItems(Id SForderId) {

        Boolean isSuccess;
        List<Order> orders = DAL_Order.getOrderById(SForderId);
        Order currentOrder = new Order();
        if (!orders.isEmpty()) {
            currentOrder = orders[0];
            //Check the Contract status associated to the Order first before trying to update the order status
            if (currentOrder.Contract.Status != STATUS_ACTIVATED) {
                isSuccess = false;
                throw new AuraHandledException('Please activate the Contract before activating this order');
            } else {
                // if Activated then set the orders status and other meaningful fields
                currentOrder.Status = STATUS_ACTIVATED;
                currentOrder.ActivatedById = UserInfo.getUserId();
                currentOrder.ActivatedDate = system.now();
            }

        }

        List<OrderItem> orderItems = new List<OrderItem>();
        List<OrderItem> orderItemsToUpdate = new List<OrderItem>();

        orderItems = DAL_OrderItem.getProductsByOrder(SForderId);
        List<DTO_OrderProducts.OrderProducts> allOrderProducts = new List<DTO_OrderProducts.OrderProducts>();

        //Loop through items and set the status of the OrderItems
        //Also prepare the products for the callout
        if (!orderItems.isEmpty()) {
            for (OrderItem item : orderItems) {
                item.Status__c = STATUS_ACTIVATED;
                DTO_OrderProducts.OrderProducts orderProduct = new DTO_OrderProducts.OrderProducts();
                orderProduct.code = item.Product2.ProductCode;
                orderProduct.name = item.Product2.Name;
                orderProduct.quantity = Integer.valueOf(item.Quantity);
                orderProduct.unitPrice = item.UnitPrice;
                allOrderProducts.add(orderProduct);
                orderItemsToUpdate.add(item);
            }

            try {
                //Create DTO with all relative information
                DTO_OrderProducts orderDTO = new DTO_OrderProducts();
                orderDTO.accountNumber = currentOrder.Account.AccountNumber;
                orderDTO.orderNumber = currentOrder.OrderNumber;
                orderDTO.status = currentOrder.Status;
                orderDTO.orderProducts = allOrderProducts;

                //Serialise the DTO to a JSON so it can be used in the call out
                String jsonString = JSON.serialize(orderDTO);
                Boolean result;

                //Set result to true for running tests as we test the callout in another class
                if(Test.isRunningTest()){
                    result = true;
                } else {
                    result = API_SendActivatedOrder.makePostCallout(WOLF_ENDPOINT,jsonString);
                }

                if (currentOrder != null && result) {
                    update currentOrder;
                    update orderItemsToUpdate;
                    isSuccess = true;
                }
            } catch (Exception e) {
                isSuccess = false;
                throw new AuraHandledException('An error has occurred :' + e);
            }
        }
        return isSuccess;

    }

}