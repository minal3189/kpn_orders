/**
 * @description Test class to test CTRL_ProductList  functionality
 * @author Minal
 * @date 20210614
 */
@IsTest
public class TEST_CTRL_ProductList {
    private static final String DRAFT_STATUS = 'Draft';
    private static final Integer CONTRACT_TERM = 24;

    /**
     * @description setup pricebooks and accounts
     */
    @TestSetup
    static void makeData() {

        FACTORY_DataFactory.createAccounts();
        FACTORY_DataFactory.SetupProductsAndPricebooks();

    }

    /**
     * @description test adding products to the order
     */
    @IsTest
    static void test_addProductsToOrder() {

        setupContractAndOrder();
        Order ord = [SELECT Id FROM Order LIMIT 1];
        Product2 product = [SELECT Id FROM Product2 LIMIT 1];
        PricebookEntry priceEntry = [SELECT Id FROM PricebookEntry WHERE Product2Id = :product.Id LIMIT 1];

        FACTORY_DataFactory.createOrderProducts(ord.Id, product.Id, priceEntry.Id);

        List<PricebookEntry> priceEntries = [SELECT Id, Product2Id, UnitPrice, Product2.Name, Product2.Id FROM PricebookEntry];

        try {
            // We should be getting 2 entires here
            Boolean result = CTRL_ProductList.addProductsToOrder(ord.Id, priceEntries);
            System.assertEquals(true, result, 'Result returned false');

        } catch (Exception e) {

        }

        try {
            // We can now get all entries/ products for this order
            List<PricebookEntry> allEntries = CTRL_ProductList.getProductsAndEntriesByOrder(ord.Id);
            System.assertEquals(true, !allEntries.isEmpty(), 'List does not contain entries');

        } catch (Exception e) {

        }

    }

    /**
     * @description Sets up contracts and order
     */
    private static void setupContractAndOrder() {

        Account selectedAccount = [SELECT Id, Name FROM Account LIMIT 1];
        Id pbID = Test.getStandardPricebookId();
        Contract contr = FACTORY_DataFactory.createContract(selectedAccount, DRAFT_STATUS, System.today(), CONTRACT_TERM, pbID)[0];
        Order ord = FACTORY_DataFactory.createOrder(selectedAccount, DRAFT_STATUS, System.today(), contr.Id)[0];
    }
}