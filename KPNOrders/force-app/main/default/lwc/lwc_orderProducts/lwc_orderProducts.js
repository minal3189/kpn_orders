import { LightningElement, track, wire, api } from 'lwc';
import getOrderItems from '@salesforce/apex/CTRL_OrderProducts.getOrderItems';
import activateOrderAndOrderItems from '@salesforce/apex/CTRL_OrderProducts.activateOrderAndOrderItems';
import { getRecord, getRecordNotifyChange, getFieldValue } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import ORDER_STATUS_FIELD from "@salesforce/schema/Order.Status";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const columns = [
    { label: 'Product', fieldName: 'Product2.Name', sortable: 'true' },
    { label: 'Unit Price', fieldName: 'UnitPrice', sortable: 'true' },
    { label: 'Quantity', fieldName: 'Quantity', sortable: 'true' },
    { label: 'Total Price', fieldName: 'TotalPrice', sortable: 'true' }
];

export default class Lwc_orderProducts extends LightningElement {

    error;
    displayNoRowMessage = false;
    isActivated;
    columns = columns;
    allOrderProducts;
    orderProductList = [];
    orderItem;
    isLoading = false;
    @api recordId;

    @wire(getRecord, { recordId: '$recordId', fields: ['Order.OrderNumber', 'Order.Name', 'Order.Status'] })
    getOrderRecord({ data, error }) {
        console.log('OrderItem => ', data, error);
        if (data) {
            this.orderItem = data;
            console.log('Order => ', JSON.stringify(this.orderItem));
            this.isLoading = true;
            this.refreshProductList();
            this.isLoading = false;
        } else if (error) {
            this.isLoading = false;
            console.error('ERROR => ', JSON.stringify(error));
            const evt = new ShowToastEvent({
                title: 'Error loading initial data',
                message: error.body.message,
                variant: 'error',
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
        }
    }

    get orderStatus() {
        var status = getFieldValue(this.orderItem, ORDER_STATUS_FIELD);

        if (status === 'Activated') {
            this.isActivated = true;
        } else {
            this.isActivated = false;
        }
        return status;
    }

    refreshProductList() {
        console.log('refreshProductList for => ', JSON.stringify(this.orderItem));
        return refreshApex(this.orderProductList);
    }

    @wire(getOrderItems, { SForderId: '$recordId' })
    orderProducts(result) {
        this.isLoading = true;
        this.orderProductList = result;

        if (result.data) {
            let productArray = [];
            for (let row of result.data) {
                // this const stroes a single flattened row. 
                const flattenedRow = {}

                // get keys of a single row — Name, Phone, LeadSource and etc
                let rowKeys = Object.keys(row);

                //iterate 
                rowKeys.forEach((rowKey) => {

                    //get the value of each key of a single row. John, 999-999-999, Web and etc
                    const singleNodeValue = row[rowKey];

                    //check if the value is a node(object) or a string
                    if (singleNodeValue.constructor === Object) {

                        //if it's an object flatten it
                        this._flatten(singleNodeValue, flattenedRow, rowKey)
                    } else {

                        //if it’s a normal string push it to the flattenedRow array
                        flattenedRow[rowKey] = singleNodeValue;
                    }

                });

                //push all the flattened rows to the final array 
                productArray.push(flattenedRow);
            }

            this.allOrderProducts = productArray;
            console.log(' length of order products ' + this.allOrderProducts.length);
            if (this.allOrderProducts.length === 0) {
                this.displayNoRowMessage = true;
            } else {
                this.displayNoRowMessage = false;
            }
            this.error = undefined;
            this.isLoading = false;
        } else if (result.error) {
            this.error = result.error;
            this.isLoading = false;
            const evt = new ShowToastEvent({
                title: 'Error loading initial data',
                message: this.error.body.message,
                variant: 'error',
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
            this.allOrderProducts = undefined;
        }
    }

    _flatten = (nodeValue, flattenedRow, nodeName) => {
        let rowKeys = Object.keys(nodeValue);
        rowKeys.forEach((key) => {
            let finalKey = nodeName + '.' + key;
            flattenedRow[finalKey] = nodeValue[key];
        })
    }

    handleActiveOrder() {
        this.isLoading = true;
        activateOrderAndOrderItems({ SForderId: this.recordId })
            .then(result => {

                if (result) {
                    this.isActivated = result;
                    const evt = new ShowToastEvent({
                        title: 'Success!',
                        message: 'Order was Activated Successfully',
                        variant: 'success',
                        mode: 'dismissable'
                    });
                    this.dispatchEvent(evt);
                    getRecordNotifyChange([{ recordId: this.recordId }]);
                    this.isLoading = false;
                }
            })
            .catch(error => {
                this.isLoading = false;
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: error.body.message,
                    variant: 'error',
                    mode: 'dismissable'
                });
                this.dispatchEvent(evt);
                console.log('Couldnt activate the order ' + JSON.stringify(error.body.message));
            })
    }


    handleSortdata(event) {
        // field name
        this.sortBy = event.detail.fieldName;

        // sort direction
        this.sortDirection = event.detail.sortDirection;

        // calling sortdata function to sort the data based on direction and selected field
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
    }

    sortData(fieldname, direction) {
        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.allOrderProducts));

        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction 
        let isReverse = direction === 'asc' ? 1 : -1;

        // sorting data 
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        this.allOrderProducts = parseData;

    }

}