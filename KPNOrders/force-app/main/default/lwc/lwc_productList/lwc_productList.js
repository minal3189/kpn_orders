import { LightningElement, api, wire } from 'lwc';
import getProductsAndEntriesByOrder from '@salesforce/apex/CTRL_ProductList.getProductsAndEntriesByOrder';
import addProductsToOrder from '@salesforce/apex/CTRL_ProductList.addProductsToOrder';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';

const columns = [
    { label: 'Product Name', fieldName: 'Product2.Name' },
    { label: 'List Price', fieldName: 'UnitPrice' }
];

export default class Lwc_productList extends LightningElement {

    productList;
    @api isActivated;
    progressValue;
    columns = columns;
    allAvailableProducts = [];
    page = 1; //this will initialize 1st page
    items = []; //it contains all the records.
    startingRecord = 1; //start record position per page
    endingRecord = 0; //end record position per page
    pageSize = 10; //default value we are assigning
    totalRecountCount = 0; //total record count received from all retrieved records
    totalPage = 0; //total number of page is needed to display all records
    isLoading = false;
    selectedRows = [];
    canAddProduct = true;
    @api recordId;

    @wire(getProductsAndEntriesByOrder, { SForderId: '$recordId' })
    products(result) {
        this.isLoading = true;
        this.productList = result;


        if (result.data) {
            let productArray = [];
            for (let row of result.data) {
                // this const stroes a single flattened row. 
                const flattenedRow = {}

                // get keys of a single row — Name, Phone, LeadSource and etc
                let rowKeys = Object.keys(row);

                //iterate 
                rowKeys.forEach((rowKey) => {

                    //get the value of each key of a single row. John, 999-999-999, Web and etc
                    const singleNodeValue = row[rowKey];

                    //check if the value is a node(object) or a string
                    if (singleNodeValue.constructor === Object) {

                        //if it's an object flatten it
                        this._flatten(singleNodeValue, flattenedRow, rowKey)
                    } else {

                        //if it’s a normal string push it to the flattenedRow array
                        flattenedRow[rowKey] = singleNodeValue;
                    }

                });

                //push all the flattened rows to the final array 
                productArray.push(flattenedRow);
            }
            this.items = productArray;
            this.allAvailableProducts = productArray;
            //this.allAvailableProducts = result.data;
            this.totalRecountCount = result.data.length;
            this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize);

            this.allAvailableProducts = this.items.slice(0, this.pageSize);
            this.endingRecord = this.pageSize;

            console.log('all products ' + JSON.stringify(this.allAvailableProducts));
            this.isLoading = false;
            this.error = undefined;
        } else if (result.error) {
            this.isLoading = false;
            this.error = result.error;
            const evt = new ShowToastEvent({
                title: 'Error loading initial data',
                message: this.error.body.message,
                variant: 'error',
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
            console.error('ERROR => ', JSON.stringify(error));
        }
    }

    _flatten = (nodeValue, flattenedRow, nodeName) => {
        let rowKeys = Object.keys(nodeValue);
        rowKeys.forEach((key) => {
            let finalKey = nodeName + '.' + key;
            flattenedRow[finalKey] = nodeValue[key];
        })
    }

    addProducts() {
        this.isLoading = true;
        var selectedRecords = this.template.querySelector("lightning-datatable").getSelectedRows();

        console.log('Selected Rows are ' + JSON.stringify(selectedRecords));
        console.log('recordId are ' + JSON.stringify(this.recordId));
        addProductsToOrder({ SForderId: this.recordId, orderProductList: selectedRecords })
            .then(result => {

                console.log('result is ' + JSON.stringify(result));
                if (result) {
                    this.isLoading = false;
                    this.setSelectedRows = [];
                    this.canAddProduct = true;
                    this.dispatchEvent(
                        new CustomEvent('addedproductschange'));
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success!',
                            message: 'Product has been added successfully',
                            variant: 'success',
                            mode: 'dismissable'
                        })
                    );
                    this.isLoading = false;
                }
            })
            .catch(error => {
                this.isLoading = false;
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: error.body.message,
                    variant: 'error',
                    mode: 'dismissable'
                });
                this.dispatchEvent(evt);
                console.log('Couldnt add the product' + JSON.stringify(error));
            })
    }

    previousHandler() {

        if (this.page > 1) {
            this.isLoading = true;
            this.page = this.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.page);
            this.isLoading = false;
        }
    }

    nextHandler() {

        if ((this.page < this.totalPage) && this.page !== this.totalPage) {
            this.isLoading = true;
            this.page = this.page + 1; //increase page by 1
            this.displayRecordPerPage(this.page);
            this.isLoading = false;
        }
    }

    displayRecordPerPage(page) {

        /*let's say for 2nd page, it will be => "Displaying 6 to 10 of 23 records. Page 2 of 5"
        page = 2; pageSize = 5; startingRecord = 5, endingRecord = 10
        so, slice(5,10) will give 5th to 9th records.
        */
        this.startingRecord = ((page - 1) * this.pageSize);
        this.endingRecord = (this.pageSize * page);

        this.endingRecord = (this.endingRecord > this.totalRecountCount)
            ? this.totalRecountCount : this.endingRecord;

        this.allAvailableProducts = this.items.slice(this.startingRecord, this.endingRecord);

        //increment by 1 to display the startingRecord count, 
        //so for 2nd page, it will show "Displaying 6 to 10 of 23 records. Page 2 of 5"
        this.startingRecord = this.startingRecord + 1;
    }

    handleOnRowSelect(event) {
        const selectedRows = event.detail.selectedRows;
        console.log('the rows selected are: ' + selectedRows);

        if (selectedRows.length > 0) {
            this.canAddProduct = false;
        } else {
            this.canAddProduct = true;
        }

    }

}